const apiPath = 'http://192.168.1.17:1337/users/';
const playlistPath = 'http://localhost:1337/playlist/';
const songsPath = 'http://localhost:1337/songs/';

export const environment = {
  production: true,
  login: apiPath + 'login',
  validateToken: apiPath + 'validate',
  playList: playlistPath + 'allplaylist',
  addPlaylist: playlistPath + 'addplaylist',
  songs: songsPath + 'allsongs',
};
