export interface Songs {
  title: string;
  link: string;
  artist: string;
  duration: number;
  description: string;
}
