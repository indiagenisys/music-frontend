export interface IUser {
  name: string;
  password: string;
  email: string;
  mobileNo: string;
  profilePicture?: string;
  role?: string;
}
