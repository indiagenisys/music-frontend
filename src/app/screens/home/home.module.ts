import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { RouterModule, Routes } from '@angular/router';
import { MaterialUiModule } from 'src/app/_config/common/material.modules';
import { CommonModules } from 'src/app/_config/common/common.module';

const routes: Routes = [{ path: '', component: HomeComponent }];

@NgModule({
  declarations: [HomeComponent],
  imports: [RouterModule.forChild(routes), MaterialUiModule, CommonModules],
})
export class HomeModule {}
