import { Component, EventEmitter, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Songs } from '@app/_interfaces';
import { AuthenticationService } from '@app/_services';
import { PlaylistService } from '@app/_services/playlist/playlist.service';
import { Playlist } from '@app/_services/playlists/playlist.model';
import { Track } from 'ngx-audio-player';
import { SavePlaylistComponent } from '../common/save-playlist/save-playlist.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  playlists: Playlist[] = [];
  loading: boolean = false;
  songs: any[] = [];
  allSongs: any[] = [];
  playerSongs: Track[] = [];
  currentIndex = 0;
  searchString: string = '';
  playlistsDetails: any[] = [];
  setPlaylistData = new EventEmitter<Array<Playlist>>();

  constructor(
    private authenticationService: AuthenticationService,
    private playlistService: PlaylistService,
    public dialog: MatDialog,
    private router: Router
  ) {}
  ngOnInit() {
    this.getAllSongs();
  }

  searchText(event: string) {
    this.playlistService.getAllSongs(event).subscribe((response: any) => {
      if (+response.status === 200 && response.songs.length) {
        this.songs = response.songs;
        this.playerSongs.push({
          title: this.songs[this.currentIndex].title,
          link: this.songs[this.currentIndex].link,
          artist: this.songs[this.currentIndex].artist,
          duration: this.songs[this.currentIndex].duration,
        });
      } else {
        this.songs = [];
      }
    });
  }

  selectedPlaylist($event) {
    if ($event) {
      let allSongs = [];
      allSongs = $event;
      this.songs = allSongs;
    } else {
      this.songs = this.allSongs;
    }
  }

  setPlaylistDataForDialog($event) {
    return $event ? (this.playlistsDetails = $event) : [];
  }

  addPlaylist(selectedSong) {
    const dialogRef = this.dialog.open(SavePlaylistComponent, {
      width: '450px',
      data: {
        songs: selectedSong,
        playlist: this.playlistsDetails,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
      if (+result === 1) {
        this.setPlaylistData.emit(result);
      }
    });
  }

  getDurationOfMusic(duration: number) {
    let seconds = duration % 60;
    let foo = duration - seconds;
    let minutes = foo / 60;
    if (minutes < 10) {
      minutes = ('0' + minutes.toString()) as any;
    }
    if (seconds < 10) {
      seconds = ('0' + seconds.toString()) as any;
    }
    let fixedCurrentTime = minutes + ':' + seconds;
    return fixedCurrentTime;
  }

  nextSongPlay() {
    // Check whether the next index data is available or not.
    if (!this.songs[this.currentIndex + 1]) {
      this.currentIndex = -1;
    }
    this.playerSongs = [];
    this.playerSongs = [
      {
        title: this.songs[this.currentIndex + 1].title,
        link: this.songs[this.currentIndex + 1].link,
        artist: this.songs[this.currentIndex + 1].artist,
        duration: this.songs[this.currentIndex + 1].duration,
      },
    ];
    this.currentIndex++;
  }

  selectedMusic(song, index) {
    this.currentIndex = index;
    this.playerSongs = [];
    this.playerSongs = [
      {
        title: song.title,
        link: song.link,
        artist: song.artist,
        duration: song.duration,
      },
    ];
  }

  getAllSongs() {
    this.loading = true;
    this.playlistService.getAllSongs(null).subscribe((response: any) => {
      this.loading = false;
      if (+response.status === 200) {
        this.songs = response.songs;
        this.allSongs = response.songs;
        this.playerSongs.push({
          title: this.songs[this.currentIndex]?.title,
          link: this.songs[this.currentIndex]?.link,
          artist: this.songs[this.currentIndex]?.artist,
          duration: this.songs[this.currentIndex]?.duration,
        });
      } else {
        this.songs = [];
      }
    });
  }
}
