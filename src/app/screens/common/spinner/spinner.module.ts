import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from './spinner.component';
import { MaterialUiModule } from '@app/_config';

@NgModule({
  declarations: [SpinnerComponent],
  imports: [CommonModule, MaterialUiModule],
  exports: [SpinnerComponent],
  entryComponents: [SpinnerComponent],
})
export class SpinnerModule {}
