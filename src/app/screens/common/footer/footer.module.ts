import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer.component';
import { NgxAudioPlayerModule } from 'ngx-audio-player';

@NgModule({
  declarations: [FooterComponent],
  imports: [CommonModule, NgxAudioPlayerModule],
  exports: [FooterComponent],
  entryComponents: [FooterComponent],
})
export class FooterModule {}
