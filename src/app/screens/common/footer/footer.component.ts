import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Track } from 'ngx-audio-player';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  msaapDisplayTitle = true;
  msaapDisplayPlayList = false;
  msaapPageSizeOptions = [2, 4, 6];
  msaapDisplayVolumeControls = true;
  msaapDisplayRepeatControls = true;
  msaapDisplayArtist = true;
  msaapDisplayDuration = true;
  msaapDisablePositionSlider = false;
  currentTrack: Track = null;
  pageSizeOptions = [2, 4, 6];
  msaapPlaylist: Track[] = [];
  volume: number = 100;
  @Input() songs;
  @Output() playNextSong = new EventEmitter();

  constructor() {}

  ngOnInit(): void {
  }

  onEnded(event) {
    this.playNextSong.emit();
    console.log(event);
    // this.currentTrack = null;
  }
}
