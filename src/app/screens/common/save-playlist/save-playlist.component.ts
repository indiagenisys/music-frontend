import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PlaylistService } from '@app/_services/playlist/playlist.service';

@Component({
  selector: 'app-save-playlist',
  templateUrl: './save-playlist.component.html',
  styleUrls: ['./save-playlist.component.scss'],
})
export class SavePlaylistComponent implements OnInit {
  playlistName: string = '';
  submitted: boolean = false;
  constructor(
    private playlistService: PlaylistService,
    private matDialogRef: MatDialogRef<SavePlaylistComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    console.log('dddd', this.data);
  }

  savePlaylist() {
    let savePlaylist = {
      name: this.playlistName,
      playlistSongs: [this.data.songs._id],
    };
    this.submitted = true;
    if (!this.playlistName || this.playlistName === '') {
      return;
    }
    this.playlistService
      .savePlaylist(savePlaylist)
      .subscribe((response: any) => this.matDialogRef.close(1));
  }
}
