import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialUiModule } from '@app/_config';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SavePlaylistComponent } from './save-playlist.component';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [SavePlaylistComponent],
  imports: [
    CommonModule,
    MaterialUiModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [SavePlaylistComponent],
  entryComponents: [SavePlaylistComponent],
})
export class SavePlaylistModule {}
