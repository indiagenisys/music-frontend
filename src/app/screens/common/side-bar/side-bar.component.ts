import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Songs } from '@app/_interfaces';
import { PlaylistService } from '@app/_services/playlist/playlist.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss'],
})
export class SideBarComponent implements OnInit {
  playlists = [];
  @Output() setSelectedPlaylist = new EventEmitter<Array<Songs>>();
  @Output() playlistsDetails = new EventEmitter<Array<Songs>>();
  constructor(private playlistService: PlaylistService) {}

  ngOnInit(): void {
    this.getPlaylists();
  }

  getPlaylists(): void {
    this.playlistService.getPlaylists().subscribe((response: any) => {
      this.playlistsDetails.emit(response.playlists);
      this.playlists = response.playlists;
    });
  }

  selectplaylist(playlist) {
    this.setSelectedPlaylist.emit(playlist.playlistSongs);
  }
}
