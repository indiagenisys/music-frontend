import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from '../footer/footer.component';
import { HeaderComponent } from './header.component';
import { FormsModule } from '@angular/forms';
import { MaterialUiModule } from '@app/_config';

@NgModule({
  declarations: [HeaderComponent],
  imports: [CommonModule, FormsModule, MaterialUiModule],
  exports: [HeaderComponent],
  entryComponents: [HeaderComponent, FooterComponent],
})
export class HeaderModule {}
