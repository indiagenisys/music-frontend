import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AuthenticationService } from '@app/_services';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public searchSongs$ = new Subject<string>();
  private onSearchChangeSubscription: Subscription;
  searchSongs = '';
  @Output() searchText = new EventEmitter<String>();
  constructor(private authenticationService: AuthenticationService) {}

  ngOnInit(): void {
    this.onSearchChangeSubscription = this.searchSongs$
      .pipe(
        tap(() => ({})),
        debounceTime(600),
        distinctUntilChanged()
      )
      .subscribe(
        (searchValue: string) => {
          this.searchSongs = searchValue;
          this.searchText.emit(this.searchSongs);
        },
        (error) => {}
      );
  }

  logout() {
    this.authenticationService.logout();
  }
}
