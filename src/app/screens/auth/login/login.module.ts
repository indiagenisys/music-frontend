import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { RouterModule, Routes } from '@angular/router';
import { MaterialUiModule } from 'src/app/_config/common/material.modules';
import { CommonModules } from '@app/_config';

const routes: Routes = [{ path: '', component: LoginComponent }];

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    MaterialUiModule,
    RouterModule.forChild(routes),
    CommonModules,
  ],
  exports: [LoginComponent],
})
export class LoginModule {}
