import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxAudioPlayerModule } from 'ngx-audio-player';
import { FooterModule } from '@app/screens/common/footer/footer.module';
import { HeaderModule } from '@app/screens/common/header/header.module';
import { SideBarModule } from '@app/screens/common';
import { CommonModule } from '@angular/common';
import { SpinnerModule } from '@app/screens/common/spinner/spinner.module';
import { SavePlaylistModule } from '@app/screens/common/save-playlist/save-playlist.module';

@NgModule({
  declarations: [],
  exports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    NgxAudioPlayerModule,
    HeaderModule,
    FooterModule,
    SideBarModule,
    SavePlaylistModule,
    SpinnerModule,
  ],
})
export class CommonModules {}
