import { createAction, props } from '@ngrx/store';
import { Playlist } from './playlist.model';

export const PLAYLIST = '[Playlist page] playlist';
export const PLAYLIST_SUCCESS = '[Playlist page] playlist successfully';
export const PLAYLIST_FAILED = '[Playlist page] playlist failed';

export const playlist = createAction(PLAYLIST, props<{ playlist: string }>());
export const playlist_success = createAction(
  PLAYLIST_SUCCESS,
  props<{ playlist: Playlist }>()
);
export const playlist_failed = createAction(
  PLAYLIST_FAILED,
  props<{ message: string }>()
);
