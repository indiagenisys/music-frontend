import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { PlaylistService } from '../playlist/playlist.service';

@Injectable({ providedIn: 'root' })
export class PlaylistEffects {
  constructor(
    private $actions: Actions,
    private playlistService: PlaylistService
  ) {}
}
