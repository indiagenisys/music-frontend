import { Songs } from '../../_interfaces';

export interface Playlist {
  name: string;
  songs: Array<Songs> | Songs[];
}
