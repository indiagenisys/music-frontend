import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Playlist } from '../playlists/playlist.model';

@Injectable({
  providedIn: 'root',
})
export class PlaylistService {
  private playlistSubject: BehaviorSubject<Playlist>;
  public playlist: Observable<Playlist>;

  constructor(private http: HttpClient) {}

  getPlaylists(): Observable<Playlist[]> {
    return this.http.get<any>(environment.playList).pipe(
      map((playlist) => {
        return playlist;
      })
    );
  }

  getAllSongs(searchText: string): Observable<any> {
    let searchString = '';
    if (searchText && searchText !== '') {
      searchString = `?search=${searchText}`;
    }
    return this.http.get<any>(environment.songs + searchString).pipe(
      map((songs) => {
        return songs;
      })
    );
  }

  savePlaylist(savePlaylist: any) {
    return this.http.post<any>(environment.addPlaylist, savePlaylist).pipe(
      map((playlists) => {
        return playlists;
      })
    );
  }
}
